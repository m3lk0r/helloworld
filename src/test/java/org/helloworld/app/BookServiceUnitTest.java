package org.helloworld.app;

import java.util.List;

import org.helloworld.app.models.Book;
import org.helloworld.app.services.BookService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BookServiceUnitTest {

	@Autowired
	private BookService bookService;

	@Test
	public void whenApplicationStarts_thenHibernateCreatesInitialRecords() {
		List<Book> books = bookService.list();

		Assert.assertEquals(books.size(), 3);
	}
}
