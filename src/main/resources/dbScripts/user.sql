/* https://spring.io/guides/gs/accessing-data-mysql/ */
create database helloWorldDB;
create user 'java'@'%' identified by 'password';
grant all on helloWorldDB.* to 'java'@'%';
