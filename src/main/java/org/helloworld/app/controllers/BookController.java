package org.helloworld.app.controllers;

import java.util.List;

import org.helloworld.app.models.Book;
import org.helloworld.app.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
class BookController {

	@Autowired
	private BookRepository repository;

	@GetMapping("/books")
	List<Book> all() {
		log.info("hello world");
		return repository.findAll();
	}
}