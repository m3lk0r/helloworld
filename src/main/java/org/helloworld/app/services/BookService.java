package org.helloworld.app.services;

import java.util.List;

import org.helloworld.app.models.Book;
import org.helloworld.app.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {

	@Autowired
	private BookRepository bookRepository;

	public List<Book> list() {
		return bookRepository.findAll();
	}
}
