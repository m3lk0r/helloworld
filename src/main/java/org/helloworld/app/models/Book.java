package org.helloworld.app.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "book")
@Data
public class Book {

	@Id
	@GeneratedValue
	Long id;

	String name;
}
